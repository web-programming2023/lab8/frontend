import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useLoadingStore = defineStore('loading', () => {
  const isLoading = ref(false)
  const doLoading = () => {
    isLoading.value = true
  }
  const finishLoading = () => {
    isLoading.value = false
  }
  return { isLoading, doLoading, finishLoading }
})
