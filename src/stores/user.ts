import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/user'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
  const loadingStore = useLoadingStore()
  const users = ref<User[]>([])

  //Data function
  //initialize is load data from back-end
  async function getUser(id: number) {
    loadingStore.doLoading()
    const res = await userService.getUser(id)
    users.value = res.data
    loadingStore.finishLoading()
  }
  async function getUsers() {
    loadingStore.doLoading()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finishLoading()
  }
  async function saveUser(user: User) {
    loadingStore.doLoading()
    if (user.id < 0) {
      //Add new
      console.log('Post' + JSON.stringify(user))
      const res = await userService.addUser(user)
    } else {
      // Update
      console.log('patch' + JSON.stringify(user))
      const res = await userService.updateUser(user)
    }
    await getUsers()
    loadingStore.finishLoading()
  }

  async function deleteUser(user: User) {
    loadingStore.doLoading()
    const res = await userService.delUser(user)
    await getUsers()
    loadingStore.finishLoading()
  }
  return { users, getUsers, getUser, saveUser, deleteUser }
})
